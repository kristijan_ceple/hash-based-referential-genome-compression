#include "Decompressor.h"
#include <vector>
#include <variant>
#include <algorithm>
#include <filesystem>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
using namespace std;
namespace fs = filesystem;


/**
This is actually a tester function -> wanna see if greedy matching really works!
Later will need to a write a function that takes only RGC data.

Restores a numeric vector, instead of a character one.
*/
bool Decompressor::restoreNumericSequence(Chromosome& toRestore)
{
	// Okay, let's reverse it. First check if there even are the compr results available
	vector<variant<Interval, uint8_t>>& greedyMatchingRes = toRestore.getGreedyMatchingResults();
	if (greedyMatchingRes.size() == 0) {
		return false;
	}

	// Let's start by fetching the reference then, and preparing the restored seq vector in the table
	vector<uint8_t>& refSequence = toRestore.getReferenceSequence();
	vector<uint8_t>& restoredSeq = this->decompressedChromosomeNumSequences.emplace(toRestore.getName(), vector<uint8_t>{}).first->second;
	
	vector<variant<Interval, uint8_t>>& greedyMatchRes = toRestore.getGreedyMatchingResults();
	for_each(greedyMatchRes.begin(), greedyMatchRes.end(), [&restoredSeq, &refSequence](variant<Interval, uint8_t>& _in) {
		visit(RestorationVisitor{restoredSeq, refSequence}, _in);
	});
	return true;
}

bool Decompressor::restoreCharacterSequence(Chromosome& toRestore)
{
	string name = toRestore.getName();

	// First check if this chromosome has even been restored to numeric sequence
	auto it = this->decompressedChromosomeNumSequences.find(name);
	if (it == this->decompressedChromosomeNumSequences.end()) {
		return false;
	}
	vector<uint8_t>& restoredNumSeq = it->second;					// Else proceed to restore the sequence - fetch the restored numeric sequence

	// Check if it's maybe already been decompressed
	auto res = this->decompressedChromosomeCharSequences.try_emplace(name);
	if (!res.second) {
		// False -- insertion did not happen, which means that entry was already present
		return false;
	}

	vector<char>& restoredCharSeq = res.first->second;				// Produce, and grab the new restoredCharSeq
	// string restoredCharSeqStr;		--DEBUG
	/*
	The algorithm goes as follows:
	1. pass: Other characters, NIntervals and normal sequence chars
	2. pass: Lowercase

	Because other chars and NIntervals were sequentially stored, it's enough to keep one cached while slowly reconstructing
	the target sequence!
	*/
	// ###########################################			CACHE			################################################
	vector<Interval>& Nintvs = toRestore.getNIntervals();
	vector<OtherChar>& otherChars = toRestore.getOtherCharacters();
	vector<Interval>& lwcs = toRestore.getLwcIntervals();
	
	unsigned int* currLwcN = nullptr;
	unsigned int* currLwcPos = nullptr;

	unsigned int indexCached = 0;
	auto NintvsIter = Nintvs.begin();
	auto otherCharsIter = otherChars.begin();

	auto NintvsEnd = Nintvs.end();
	auto otherCharsEnd = otherChars.end();
	// ###########################################			CACHE			################################################
	
	// 1st Pass: Other characters, NIntervals and normal sequence chars
	unsigned int restoredNumSeqN = restoredNumSeq.size();
	unsigned int charIndex = 0;
	for (unsigned int numIndex = 0; numIndex < restoredNumSeqN; charIndex++) {
		// Check if 'N' was present at this location
		if (NintvsIter != NintvsEnd && NintvsIter->pos == charIndex) {
			// Restore N
			unsigned int intvN = NintvsIter->len;
			for (unsigned int i = 0; i < intvN; i++, charIndex++) {
				restoredCharSeq.push_back('N');
			}
			// Len positions have been put to N. i has been incremented to point pas len. pos, and will be incremented one more time at the start of this loop -- therefore need to counter that
			charIndex--;
			NintvsIter++;
		}
		else if (otherCharsIter != otherCharsEnd && otherCharsIter->pos == charIndex) {
			// Check if OtherChar was present at this location
			restoredCharSeq.push_back(otherCharsIter->ch);
			otherCharsIter++;
		}
		else {
			// Put 0123(ACGTacgt) - the normal alphabet
			restoredCharSeq.push_back(inverseBasesFunc(restoredNumSeq[numIndex++]));
		}
	}

	// Need to check if there were any leftover intervals!
	for (;; charIndex++) {
		bool wasInIf = false;
		
		if (NintvsIter != NintvsEnd && NintvsIter->pos == charIndex) {
			// Restore N
			unsigned int intvN = NintvsIter->len;
			for (unsigned int i = 0; i < intvN; i++, charIndex++) {
				restoredCharSeq.push_back('N');
			}
			// Len positions have been put to N. i has been incremented to point pas len. pos, and will be incremented one more time at the start of this loop -- therefore need to counter that
			charIndex--;
			NintvsIter++;
			
			wasInIf = true;
		}
		else if (otherCharsIter != otherCharsEnd && otherCharsIter->pos == charIndex) {
			// Check if OtherChar was present at this location
			restoredCharSeq.push_back(otherCharsIter->ch);
			otherCharsIter++;
			
			wasInIf = true;
		}

		if (!wasInIf) {
			break;
		}
	}

	// 2nd Pass: Lowercase
	for (Interval& lwc : lwcs) {
		currLwcN = &lwc.len;
		currLwcPos = &lwc.pos;
		for (unsigned int i = 0; i < *currLwcN; i++) {
			indexCached = *currLwcPos + i;
			restoredCharSeq[indexCached] = tolower(restoredCharSeq[indexCached]);
		}
	}

	// All done - the restored char sequence has already been added to the map(it was referenced actually)
	return true;
}

bool Decompressor::restoreString(Chromosome& toRestore)
{
	// First check if this chromosome already has a restored char sequence
	auto it = this->decompressedChromosomeCharSequences.find(toRestore.name);
	if (it == this->decompressedChromosomeCharSequences.end()) {
		return false;
	}

	auto restoredCharIter = this->decompressedChromosomeCharSequences.find(toRestore.name);
	vector<char>& restoredCharSequence = restoredCharIter->second;
	
	// First insert the fasta identifier during string emplacement!
	// Okay, now just make a single string and insert breakpoints into it
	auto restoredItr = this->decompressedChromosomeStringSequences.emplace(toRestore.name, toRestore.fastaIdentifier + "\n");
	string& restoredStr = restoredItr.first->second;			// This is the string that we have to fill


	// Now just insert line breakpoints at the specified positions
	// unsigned int vectorIndex = 0;			// Keeps track of the current index in the whole vector
	auto seqIter = restoredCharSequence.begin();		// Nah let's use an iterator instead of an index
	auto commentsIter = toRestore.comments.begin();
	auto commentsEnd = toRestore.comments.end();
	unsigned int n = toRestore.lineBreakPositions.size() + toRestore.comments.size();
	++n;			// Fasta id!
	auto& lineBreakPoints = toRestore.lineBreakPositions;

	// lineCounter starting from 1 because of fasta id!!!
	for(unsigned int lineCounter = 1, lineBreakPointIndex = 0; lineCounter < n; lineCounter++) {
		
		// Check for comments first
		if (commentsIter != commentsEnd && lineCounter == commentsIter->line) {
			restoredStr.append(commentsIter->content);
			restoredStr.push_back('\n');
			commentsIter++;
			continue;
		}
		
		// Copy loop
		unsigned int lineBrkptPos = lineBreakPoints[lineBreakPointIndex++];
		for (unsigned int counter = 0; counter < lineBrkptPos; counter++, seqIter++) {
			restoredStr.push_back(*seqIter);
		}

		// Now add the newline, since we've copied the elements located before the newline
		restoredStr.push_back('\n');
	}
	
	if(!TRAILING_NEWLINE) {
		restoredStr.pop_back();			// Erase the last newline
	}

	// All done, that should be it
	toRestore.restoredString = true;
	return true;
}

bool Decompressor::writeRestoredChr2File(Chromosome& toRestore)
{
	// Check if there even exists the string of this chromosome that is to be written to a file
	auto it = this->decompressedChromosomeStringSequences.find(toRestore.name);
	if (it == this->decompressedChromosomeStringSequences.end()) {
		return false;
	}

	// Now let's write to file!
	ofstream writeFile(this->restorationFolder + "/restored_" + toRestore.name, ios::trunc | ios::binary);
	if (!writeFile.is_open()) {
		return false;
	}

	writeFile << it->second;

	// The file has been written.
	writeFile.close();
	return true;
}

bool Decompressor::restoreChromosomeFolder(std::string decomprFolderArg, std::string refSeqPath, std::string restoreFolderArg)
{
	Util::loadRefFile(refSeqPath, this->prevRefSequence);

	this->restorationFolder = restoreFolderArg;

	bool allSuccess = true;
	int chrNamesIndex = 0;
	for (const auto& entry : fs::directory_iterator(decomprFolderArg)) {
		// First check the extension!
		auto entryPath = entry.path();

		// Check if fasta file
		auto found = fastaExtensionsSet.find(entryPath.extension().string());
		if (found == fastaExtensionsSet.end()) {
			allSuccess = false;
			cerr << "WARNING: File " << entry.path().filename() << "doesn't end in one of the supported FASTA file extensions!" << '\n';
		}

		bool subSuccess = restoreChromosome(entryPath.filename().string(), entry, this->prevRefSequence);
		if (!subSuccess) {
			cerr << "WARNING: Empty matching data chromosome!!!" << '\n';
		}
	}

	return allSuccess;
}

bool Decompressor::restoreChromosomesFolder_PairRefs(std::string decomprFolderArg, std::string refsFolderArg, std::string restoreFolderArg)
{
	// Okay, the files we need are in the folder, need to iterate through them
	// They should not be hidden, and should have one of fasta endings
	// Do for each file
	// Default decompr and refs folders are used!

	// Todo: auto-initialise size here?
	//vector<fs::path> chromosomesToRestore;

	bool allSuccess = true;
	int chrNamesIndex = 1;
	for (const auto& entry : fs::directory_iterator(decomprFolderArg)) {
		// First check the extension!
		auto entryPath = entry.path();
		
		// Check if fasta file
		auto found = fastaExtensionsSet.find(entryPath.extension().string());
		if (found == fastaExtensionsSet.end()) {
			allSuccess = false;
			cerr << "WARNING: File " << entry.path().filename() << "doesn't end in one of the supported FASTA file extensions!" << '\n';
		}

		string refPath = refsFolderArg + "/" + "chr" + to_string(chrNamesIndex++) + ".fa";
		bool subSuccess = Util::loadRefFile(refPath, this->prevRefSequence);
		if (!subSuccess) {
			cerr << fmt::format("Loading reference sequence failed!!! Couldn't load {:s}!", refPath) << '\n';
			return false;
		}

		subSuccess = restoreChromosome(entryPath.filename().string(), entry.path(), this->prevRefSequence);
		if (!subSuccess) {
			cerr << "WARNING: Empty matching data chromosome!!!" << '\n';
		}
	}

	return allSuccess;
}

bool Decompressor::restoreChromosomesFolder_OneRef(std::string decomprFolderArg, std::string refPath, std::string restoreFolderArg)
{
	// Okay, the files we need are in the folder, need to iterate through them
	// They should not be hidden, and should have one of fasta endings
	// Do for each file
	// Default decompr and refs folders are used!

	// Todo: auto-initialise size here?
	//vector<fs::path> chromosomesToRestore;

	bool subSuccess = Util::loadRefFile(refPath, this->prevRefSequence);
	if (!subSuccess) {
		cerr << fmt::format("Loading reference sequence failed!!! Couldn't load {:s}!", refPath) << '\n';
		return false;
	}

	bool allSuccess = true;
	for (const auto& entry : fs::directory_iterator(decomprFolderArg)) {
		// First check the extension!
		auto entryPath = entry.path();

		// Check if fasta file
		auto found = fastaExtensionsSet.find(entryPath.extension().string());
		if (found == fastaExtensionsSet.end()) {
			allSuccess = false;
			cerr << "WARNING: File " << entry.path().filename() << "doesn't end in one of the supported FASTA file extensions!" << '\n';
		}

		bool subSuccess = restoreChromosome(entryPath.filename().string(), entry.path(), this->prevRefSequence);
		if (!subSuccess) {
			cerr << "WARNING: Empty matching data chromosome!!!" << '\n';
		}
	}

	return allSuccess;
}

bool Decompressor::restoreChromosome(string name, std::filesystem::path chrPath, vector<uint8_t>& refSequence)
{
	ifstream readFile(chrPath);
	if (!readFile.is_open()) {
		cerr << "Could not open target chromosome file for restoration: " << chrPath.string() << endl;
		return false;
	}

	cout << "Restoring: " << chrPath.string() << '\n';
	
	// Okay, inside is AUX + GMD. Now need to restore the original chromosome from it
	// Create an empty chromosome that we will fill with data
	Chromosome& chr = this->decompressedChromosomes.emplace(name, Chromosome{ this->prevRefSequence , name }).first->second;
	string buffer = "";
	vector<string> splits_buffer;
	vector<string> buffer_tmp(2);

	// Read the file line by line now, and restore data
	///////////////////////////////////////			First - AUX			//////////////////////////////////////////
	// 1a) Fasta ID
	getline(readFile, buffer);
	chr.fastaIdentifier = buffer;

	// 2b) Comments
	getline(readFile, buffer);
	while (buffer != "") {
		// cout << "buffer = " << buffer << '\n';

		// Have to parse the number until the first ' ' (space)
		unsigned int i;
		for (i = 0; ; i++) {
			if (buffer[i] == ' ') {
				i++;		// Used for substr not to include the ' '(space)
				break;
			}
			
			buffer_tmp[0].push_back(buffer[i]);
		}

		chr.comments.emplace_back(
			Comment{
				stoul(buffer_tmp[0]),
				buffer.substr(i)
			}
		);

		buffer_tmp[0].clear();
		getline(readFile, buffer);
	}

	// 1c) Linebreak points
	// De run length encoding here -- until then can't do anything
	getline(readFile, buffer);
	if (buffer != "") {
		// Remove the last space
		buffer.pop_back();
		boost::split(splits_buffer, buffer, boost::is_any_of(" "));

		// Convert the strings to ints now
		vector<unsigned int> lineBreakPointsRLE;
		for_each(splits_buffer.begin(), splits_buffer.end(), [&lineBreakPointsRLE](const string& _in) {
			lineBreakPointsRLE.push_back((unsigned int)stoul(_in));
		});
		// Done, the method deRunLenghtEncoding takes a reference and pushes_back elements into the matrix
		deRunLenghtEncoding(chr.lineBreakPositions, lineBreakPointsRLE);

		splits_buffer.clear();			// Clean - Up
	}

	// 1d) Other Characters
	// Format <<pos,chr> <pos,chr> <pos,chr> ... <pos,chr>>
	getline(readFile, buffer);
	if (buffer != "") {
		// Remove the last space
		buffer.pop_back();
		boost::split(splits_buffer, buffer, boost::is_any_of(" "));
		for (const string& otherChar : splits_buffer) {
			// The format is <pos,chr>. Therefore now need to split by ','
			boost::split(buffer_tmp, otherChar, boost::is_any_of(","));

			chr.otherCharacters.emplace_back(
				OtherChar{
					stoul(buffer_tmp[0]),
					buffer_tmp[1][0]			// String consisting of only 1 character at position 0
				}
			);
		}
		splits_buffer.clear();
	}

	// 1e) N intervals
	// Format <<pos_in_ref,len> <pos_in_ref,len> <pos_in_ref,len> ... <pos_in_ref,len>>
	getline(readFile, buffer);
	if (buffer != "") {
		// Remove the last space
		buffer.pop_back();
		boost::split(splits_buffer, buffer, boost::is_any_of(" "));
		for (const string& NInterval : splits_buffer) {
			// The format is <pos_in_ref,len>. Therefore now need to split by ','
			boost::split(buffer_tmp, NInterval, boost::is_any_of(","));

			chr.NIntervals.emplace_back(
				Interval{
					stoul(buffer_tmp[0]),
					stoul(buffer_tmp[1])
				}
			);
		}
		splits_buffer.clear();
	}

	// 1f) Lower-Case Intervals
	// Format <<pos_in_tar,len> <pos_in_tar,len> <pos_in_tar,len> ... <pos_in_tar,len>>
	getline(readFile, buffer);
	if (buffer != "") {
		// Remove the last space
		buffer.pop_back();
		boost::split(splits_buffer, buffer, boost::is_any_of(" "));
		for (const string& lwcIntv : splits_buffer) {
			// The format is <pos_in_tar,len>. Therefore now need to split by ','
			boost::split(buffer_tmp, lwcIntv, boost::is_any_of(","));

			chr.lwcIntervals.emplace_back(
				Interval{
					stoul(buffer_tmp[0]),
					stoul(buffer_tmp[1])
				}
			);
		}
		splits_buffer.clear();
	}
	///////////////////////////////////////			First - AUX			//////////////////////////////////////////

	///////////////////////////////////////			Second - GMD			//////////////////////////////////////////
	/*
	Let's restore the chr.targetSeq and chr.refSeq member variables. Will make another function that will
	restore the string and write it to a file.

	For the current goal can use one of the finished functions. Just need to load the GMD data into the program.
	
	Format: <<pos,len> <ch[ch[ch[ch...]]]> <pos,len> <ch[ch[ch[ch...]]]> <pos,len> <ch[ch[ch[ch...]]]> ... <pos,len> <ch[ch[ch[ch...]]]>...>
	The sub-format is <pos,len>|<ch[ch[ch[ch...]]]>. Don't think there's a need for regexes here, but rather can make use of the mechanism
	described in the following paragraph.

	Can make use of the fact that there will ALWAYS follow a diff after a simil.
	Therefore, preprocess by first checking the 0th element and set the starting bool, which we can then
	alternate until the very end.
	*/
	getline(readFile, buffer);
	boost::split(splits_buffer, buffer, boost::is_any_of(" "));

	// Check if there were even any matches
	if (splits_buffer.size() == 0) {
		cerr << "No greedy matching results found!" << '\n';
		return false;
	}

	// Preprocess the 0th element
	bool simil = true;
	const string& gmdPart = splits_buffer[0];
	
	// Check if there is a comma and pre-set the simil
	if (gmdPart.find(',') == gmdPart.npos) {			// No comma found, therefore this is not a similarity written as the first part of the string
		simil = false;
	}

	for (int i = 0; i < splits_buffer.size(); i++) {
		const string& gmdPart = splits_buffer[i];
		if (simil) {
			// Format: <pos,len>
			boost::split(buffer_tmp, gmdPart, boost::is_any_of(","));

			chr.greedyMatchingResults.emplace_back(
				Interval{
					stoul(buffer_tmp[0]),
					stoul(buffer_tmp[1])
				}
			);
		}
		else {
			// Format: <ch[ch[ch[ch...]]]>
			for (char miniGmdPart : gmdPart) {
				chr.greedyMatchingResults.emplace_back(miniGmdPart - '0');		// Since it's a char '0' = 49, have to subtract 49
			}
		}

		simil ^= 0b1;		// Don't forget to alternate it
	}
	///////////////////////////////////////			Second - GMD			//////////////////////////////////////////

	bool success;
	// Okay, the data is now in. Chromosome -> Restore Numeric Sequence -> Restore Character Sequence
	success = this->restoreNumericSequence(chr);
	if (!success) {
		cerr << "Fail at restoring numeric seq!" << '\n';
		return false;
	}

	success = this->restoreCharacterSequence(chr);
	if (!success) {
		cerr << "Fail at restoring character seq!" << '\n';
		return false;
	}

	// Got the character sequence restored now restored now. Time to restore the string as well, and then write that string to a file
	success = this->restoreString(chr);
	if (!success) {
		cerr << "Fail at restoring string!" << '\n';
		return false;
	}

	// Finally at the end, now just output the restored files!
	success = writeRestoredChr2File(chr);
	if (!success) {
		cerr << "Fail at writing!" << '\n';
		return false;
	}

	readFile.close();
	return true;
}

/*
The RLE is in the second vector. Need to decode it, and put the results into
the first vector
*/
void Decompressor::deRunLenghtEncoding(vector<unsigned int>& lineBreakPointsNonRLE, 
	vector<unsigned int>& lineBreakPointsRLE)
{
	unsigned int n = lineBreakPointsRLE.size();
	for (unsigned int i = 1; i < n; i+=2) {
		unsigned int ch = lineBreakPointsRLE[i];
		unsigned int cnt = lineBreakPointsRLE[i - 1];

		for (unsigned int j = 0; j < cnt; j++) {
			// De run length encoding sure seems way easier than doing it in the positive direction - just have to push_back char, and at that do it cnt times
			lineBreakPointsNonRLE.push_back(ch);
		}
	}
}