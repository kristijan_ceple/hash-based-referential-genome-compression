#!/bin/bash

# This script was adapted from: https://crashcourse.housegordon.org/split-fasta-files.html

rm -rf ./splitChromosomes
mkdir splitChromosomes
cd splitChromosomes

csplit -s -z "../$1" '/>/' '{*}'
#for i in xx* ; do \
#  n=$(sed 's/>// ; s/ .*// ; 1q' "$i") ; \
#  mv "$i" "$n.fa" ; \
# done

counter=1
for i in xx*
do
  mv "$i" "chr${counter}.fa"
  ((counter++))
done