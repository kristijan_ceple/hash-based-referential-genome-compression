#include "DataHandler.h"
#include "Compressor.h"
#include "Decompressor.h"
#include <iostream>
#include <chrono>
#include <filesystem>
using namespace std;
namespace fs = std::filesystem;
using namespace std::chrono;

/*
Used for all core chromosome compression operations. Doesn't zip. Does a pairwise chr-chr compression.
*/
bool DataHandler::_chromosomeCompressionCore(std::string& refPath, std::string& tarPath, std::string& out, Compressor& compr, uint8_t count)
{
	// Load the references and targets
	bool success;

	success = compr.loadRefFile(refPath);
	if (!success) {
		cerr << "Reference not loaded successfully!" << '\n';
		return false;
	}

	Chromosome* chr = compr.loadChromosome(tarPath, "chr" + to_string(count));
	if (!chr) {
		return false;
	}

	
	success = compr.preprocessRef();			// Ref and target loaded! Construct the hashtable!
	if (!success) {
		cerr << "Failure during hash table construction!" << '\n';
		return false;
	}

	// Done!
	compr.compressChromosome(*chr);				// Now do the greedy matching
	compr.writeChr2File(*chr, out);				// Write the results to a file
	return true;
}
/*
	Overloaded func. Doesn't do pair-wise, but rather one chr - all other chrs compression. 
 */
bool  DataHandler::_chromosomeCompressionCore(std::string& tarPath, std::string& out, Compressor& compr, uint8_t count)
{
	if (!compr.isRefLoaded() || !compr.isHashTableConstructed()) {
		cerr << "Either reference wasn't loaded prior to compression or hash table construction has failed!" << '\n';
		return false;
	}

	Chromosome* chr = compr.loadChromosome(tarPath, "chr" + to_string(count));
	if (!chr) {
		return false;
	}

	// Done!
	compr.compressChromosome(*chr);				// Now do the greedy matching
	compr.writeChr2File(*chr, out);				// Write the results to a file
	return true;
}

/*
Used with -m chr mode.
*/
void DataHandler::chromosomeCompression(std::string& refPath, std::string& tarPath, std::string& out)
{
	// Load the references and targets
	Compressor compr;
	bool success = this->_chromosomeCompressionCore(refPath, tarPath, out, compr);
	if (!success) {
		cerr << "Failure during chromosome mode compression!" << '\n';
		return;
	}

	compr.zipFolder(out);
}

void DataHandler::genomeCompression_PairRefs(std::string& refPath, std::string& tarPath, std::string& out)
{
	Compressor compr;

	// Need to load one-by-one chromosome pairs
	const string chr = "chr";
	string chrRef, chrTar;
	const string formatTmp = "{:s}/{:s}{:d}.fa";
	for (uint8_t chrNamesIndex = 1; chrNamesIndex <= DataHandler::chrNum; chrNamesIndex++) {
		chrRef = fmt::format(formatTmp, refPath, chr, chrNamesIndex);
		chrTar = fmt::format(formatTmp, tarPath, chr, chrNamesIndex);

		// Now attempt to load the files
		bool success = this->_chromosomeCompressionCore(chrRef, chrTar, out, compr, chrNamesIndex);
		if (!success) {
			cerr << fmt::format("Failure during genome mode compression on chromosome {:s}{:d}!", chr, chrNamesIndex) << '\n';
			return;
		}

		// Delete the current chr, so as not to take up unnecessary space
		compr.chromosomes.pop_back();
	}

	// Just zip the folder
	compr.zipFolder(out);
}

void DataHandler::genomeCompression_OneRef(std::string& refPath, std::string& tarPath, std::string& out)
{
	Compressor compr;
	bool success = compr.loadRefFile(refPath);
	if (!success) {
		cerr << "Fail during reference file loading!" << '\n';
		return;
	}

	success = compr.preprocessRef();
	if (!success) {
		cerr << "Failure during hash table construction for the reference sequence!" << '\n';
	}

	// Need to load one-by-one chromosomes - while using only one ref
	const string chr = "chr";
	string chrTar;
	const string formatTmp = "{:s}/{:s}{:d}.fa";
	for (uint8_t chrNamesIndex = 1; chrNamesIndex <= DataHandler::chrNum; chrNamesIndex++) {
		chrTar = fmt::format(formatTmp, tarPath, chr, chrNamesIndex);

		// Now attempt to load the files
		bool success = this->_chromosomeCompressionCore(chrTar, out, compr, chrNamesIndex);
		if (!success) {
			cerr << fmt::format("Failure during genome mode decompression on chromosome {:s}{:d}!", chr, chrNamesIndex) << '\n';
			return;
		}

		// Delete the current chr, so as not to take up unnecessary space
		compr.chromosomes.pop_back();
	}

	// Just zip the folder
	compr.zipFolder(out);
}

void DataHandler::genomeSetCompression(std::string& refPath, std::string& tarPath, std::string& out)
{
	// RefPath is a reference genome! Let's load
	cout << "Not yet implemented" << '\n';
}

void DataHandler::chromosomeDeCompression(std::string& refPath, std::string& tarPath, std::string& out)
{
	//cout << "Before cutting: " << tarPath << '\n';
	cutExtension(tarPath);				// Detract the extension from the tarPath
	//cout << "After cutting: " << tarPath << '\n';

	Decompressor decompr;

	// First unzip
	decompr.deZipFolder(tarPath);		// Target chromosomes in the folder named the same as the zip

	bool success = decompr.restoreChromosomeFolder(tarPath, refPath, out);
	if (!success) {
		cerr << "Failure during decompression!" << '\n';
		return;
	}

	// That's it
}

void DataHandler::genomeDeCompression_PairRefs(std::string& refPath, std::string& tarPath, std::string& out)
{
	Decompressor decompr;
	decompr.restorationFolder = out;

	// First unzip
	decompr.deZipFolder(tarPath);		// Target chromosomes in the folder named the same as the zip

	bool success = decompr.restoreChromosomesFolder_PairRefs(tarPath, refPath, out);
	if (!success) {
		cerr << "Failure during decompression!" << '\n';
		return;
	}

	// That's it
}

void DataHandler::genomeDeCompression_OneRef(std::string& refPath, std::string& tarPath, std::string& out)
{
	Decompressor decompr;
	decompr.restorationFolder = out;

	// First unzip
	decompr.deZipFolder(tarPath);		// Target chromosomes in the folder named the same as the zip

	bool success = decompr.restoreChromosomesFolder_OneRef(tarPath, refPath, out);
	if (!success) {
		cerr << "Failure during decompression!" << '\n';
		return;
	}

	// That's it
}

void DataHandler::genomeSetDeCompression(std::string& refPath, std::string& tarPath, std::string& out)
{
	cout << "Not yet implemented" << '\n';
}
