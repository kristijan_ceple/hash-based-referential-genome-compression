#pragma once
#include <iostream>
#include <vector>
#include <variant>
#include <unordered_map>
#include "Util.h"
#include "HashTable.h"

enum class ChromosomeWay{ COMPR, DECOMPR };

class Chromosome
{
private:
	std::string name;
	
	std::vector<uint8_t>& refSequence;
	std::vector<uint8_t> tarSequence;

	std::string fastaIdentifier;
	std::vector<Comment> comments;
	std::vector<Interval> lwcIntervals;
	std::vector<Interval> NIntervals;
	std::vector<OtherChar> otherCharacters;
	std::vector<unsigned int> lineBreakPositions;			// Line break positions in each line, not in the whole document, e.g. instead of being 5, 10, 15 it will be 5, 5, 5
	std::vector<unsigned int> lineBreakPositionsRunLength;

	std::vector<std::variant<Interval, uint8_t>> greedyMatchingResults;
	bool greedyMatchingRun = false;
	bool restoredString = false;

	//// Copy pasted this from HiRGC 2017 source code(yuansheng)
	//inline static std::vector<std::string> defaultChromosomeNames = { "chr1.fa", "chr2.fa", "chr3.fa", "chr4.fa",
	//					"chr5.fa", "chr6.fa", "chr7.fa", "chr8.fa", "chr9.fa", "chr10.fa",
	//					"chr11.fa", "chr12.fa", "chr13.fa", "chr14.fa", "chr15.fa", "chr16.fa", "chr17.fa",
	//					"chr18.fa", "chr19.fa", "chr20.fa", "chr21.fa", "chr22.fa", "chrX.fa", "chrY.fa" };
	friend class Decompressor;
	friend class Compressor;
	friend class DataHandler;
public:
	Chromosome(std::vector<uint8_t>& refSequence, std::string& name);
	Chromosome(std::string& path, std::vector<uint8_t>& refSequence, std::string& name);
	Chromosome(std::string& path, std::vector<uint8_t>& refSequence, uint8_t defaultChromosomeNamesCounter);
	void initMemory();
	bool loadTarFile(const std::string& path);
	void greedyMatching(std::vector<std::vector<unsigned int>*>& hashTable);
	void greedyMatchingOptimised(
		std::vector<std::vector<unsigned int>*>& hashTable
		//, std::array<unsigned int, CONST_K>& tupleCoeffsCached_All
		//, unsigned int tupleCoeffsCached_Last
	);
	//bool writeResults(const std::string& path);
	void runLengthEncodingLBrks();

	inline std::string getName() { return this->name; }
	inline std::string getFastaID() { return this->fastaIdentifier; };
	inline std::vector<uint8_t>& getReferenceSequence();
	inline std::vector<uint8_t>& getTargetSequence();
	inline std::vector<OtherChar>& getOtherCharacters();
	inline std::vector<Interval>& getNIntervals();
	inline std::vector<Interval>& getLwcIntervals();
	inline std::vector<unsigned int>& getLineBreakPositions();
	inline std::vector<unsigned int>& getLineBreakPositionsRunLength();
	inline bool isGreedyMatchingRun() { return this->greedyMatchingRun; }
	inline std::vector<std::variant<Interval, uint8_t>>& getGreedyMatchingResults();
};

inline std::vector<std::variant<Interval, uint8_t>>& Chromosome::getGreedyMatchingResults()
{
	return this->greedyMatchingResults;
}

inline std::vector<uint8_t>& Chromosome::getReferenceSequence()
{
	return this->refSequence;
}

inline std::vector<uint8_t>& Chromosome::getTargetSequence()
{
	return this->tarSequence;
}

inline std::vector<Interval>& Chromosome::getNIntervals()
{
	return this->NIntervals;
}

inline std::vector<Interval>& Chromosome::getLwcIntervals()
{
	return this->lwcIntervals;
}

inline std::vector<OtherChar>& Chromosome::getOtherCharacters()
{
	return this->otherCharacters;
}

inline std::vector<unsigned int>& Chromosome::getLineBreakPositions()
{
	return this->lineBreakPositions;
}

inline std::vector<unsigned int>& Chromosome::getLineBreakPositionsRunLength()
{
	return this->lineBreakPositionsRunLength;
}