#pragma once
#include <string>
#include "Compressor.h"

class DataHandler
{
private:
	bool _chromosomeCompressionCore(std::string& refPath, std::string& tarPath, std::string& out, Compressor& compr, std::uint8_t count = 1);
	bool _chromosomeCompressionCore(std::string& tarPath, std::string& out, Compressor& compr, uint8_t count);
public:
	inline static int chrNum;

	// ##########################		Compression section		#########################################
	void chromosomeCompression(std::string& refPath, std::string& tarPath, std::string& out);
	
	void genomeCompression_PairRefs(std::string& refPath, std::string& tarPath, std::string& out);
	void genomeCompression_OneRef(std::string& refPath, std::string& tarPath, std::string& out);

	void genomeSetCompression(std::string& refPath, std::string& tarPath, std::string& out);
	// ##########################		Compression section		#########################################

		// ##########################		Decompression section		#########################################
	void chromosomeDeCompression(std::string& refPath, std::string& tarPath, std::string& out);

	void genomeDeCompression_PairRefs(std::string& refPath, std::string& tarPath, std::string& out);
	void genomeDeCompression_OneRef(std::string& refPath, std::string& tarPath, std::string& out);

	void genomeSetDeCompression(std::string& refPath, std::string& tarPath, std::string& out);
	// ##########################		Decompression section		#########################################
};

